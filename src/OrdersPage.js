import React, { useState, useEffect } from 'react';
import { GetAllOrders } from './GetRequests'; // Ensure this path is correct
import { Link } from 'react-router-dom';

const OrdersPage = () => {
    const [orders, setOrders] = useState([]);

    useEffect(() => {
        const fetchOrders = async () => {
            try {
                const fetchedOrders = await GetAllOrders();
                setOrders(fetchedOrders);
            } catch (error) {
                console.error("Error fetching orders:", error);
                // Handle error state appropriately
            }
        };

        fetchOrders();
    }, []);

    return (
        <div>
            <Link to="/" style={{ textDecoration: 'none', marginBottom: '10px' }}>
                <button>Back </button>
            </Link>
            <h1>Orders</h1>
            {orders.length > 0 ? (
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Total Price</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Customer</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {orders.map(order => (
                            <tr key={order.id}>{/* No whitespace before or after this line */}
                                <td>{order.id}</td>
                                <td>${order.totalPrice.toFixed(2)}</td>
                                <td>{new Date(order.date).toLocaleDateString()}</td>
                                <td>{order.status}</td>
                                <td>{order.customer ? order.customer.firstName : 'N/A'}</td>
                                <td>
                                    <Link to={`/orders/${order.id}`}>
                                        <button>Details</button>
                                    </Link>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            ) : (
                <p>No orders available.</p>
            )}
        </div>
    );
};

export default OrdersPage;
