import React, { useState, useEffect } from 'react';
import { GetAllProducts } from './GetRequests';
import { Link, useNavigate } from 'react-router-dom';

const ProductsPage = () => {
    const [products, setProducts] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        const fetchProducts = async () => {
            const fetchedProducts = await GetAllProducts();
            setProducts(fetchedProducts);
        };

        fetchProducts();
    }, []);

    // Функция для навигации к странице отзывов продукта
    const handleViewReviews = (productId) => {
        navigate(`/products/${productId}/reviews`);
    };

    // Функция для навигации к странице добавления отзыва продукта
    const handleAddReview = (productId) => {
        navigate(`/products/${productId}/add-review`);
    };

    return (
        <div>
            <Link to={`/`}>
                <button>Back</button>
            </Link>
            <h1>Products</h1>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Reviews</th>
                        <th>Add Review</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {products.map(product => (
                        <tr key={product.id}>
                            <td>{product.name}</td>
                            <td>{product.description}</td>
                            <td>${product.price}</td>
                            <td>
                                <button onClick={() => handleViewReviews(product.id)}>View Reviews</button>
                            </td>
                            <td>
                                <button onClick={() => handleAddReview(product.id)}>Add Review</button>
                            </td>
                            <td>
                                <Link to={`/products/${product.id}`}>
                                    <button>Details</button>
                                </Link>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
            {products.length === 0 && <p>No products available.</p>}
        </div>
    );
};

export default ProductsPage;
