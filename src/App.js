// App.js
import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import HomePage from './HomePage'; // Import your HomePage component
import ProductsPage from './ProductsPage'; // Import your ProductsPage component
import ProductDetailsPage from './ProductDetailsPage';
import OrdersPage from './OrdersPage';
import OrderDetailsPage from './OrderDetailsPage';
import './App.css';
import ProductReviewsPage from './ProductReviewsPage'; // Новый компонент для отзывов
import AddProductReviewPage from './AddProductReviewPage'; // Новый компонент для добавления отзыва


function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/products" element={<ProductsPage />} />
          <Route path="/products/:productId" element={<ProductDetailsPage />} />
          <Route path="/orders" element={<OrdersPage />} />
          <Route path="/orders/:orderId" element={<OrderDetailsPage />} />
          <Route path="/products/:productId/reviews" element={<ProductReviewsPage />} />
          <Route path="/products/:productId/add-review" element={<AddProductReviewPage />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
