// Assuming the URL is something like 'http://localhost:8080/api/products'
const PRODUCT_URL = 'http://localhost:8080/api/products';
const ORDER_URL = 'http://localhost:8080/api/orders';
const REVIEWS_URL = 'http://localhost:8080/api/reviews';

export async function GetAllProducts() {
    try {
        const response = await fetch(PRODUCT_URL);
        if (!response.ok) {
            throw new Error('Network response was not ok ' + response.statusText);
        }
        const data = response.json();
        console.log("all products: ", data);
        return await data;
    } catch (error) {
        console.error("Error fetching data: ", error);
        return []; // return empty array in case of error
    }
}


export async function GetProductById(productId) {
    try {
        const response = await fetch(PRODUCT_URL + "/" + productId + "/details"); // Make sure to use the correct endpoint for product details
        if (!response.ok) {
            throw new Error('Network response was not ok ' + response.statusText);
        }
        const data = await response.json();
        console.log("product details: ", data);
        return data; // Assuming that the data has the reviews and orders within it
    } catch (error) {
        console.error("Error fetching product details: ", error);
        // Return an object with empty arrays for reviews and orders in case of error
        return { reviews: [], orders: [] };
    }
}


export async function GetAllOrders() {
    try {
        const response = await fetch(ORDER_URL);
        if (!response.ok) {
            throw new Error('Network response was not ok ' + response.statusText);
        }
        const data = response.json();
        console.log("product: ", data);
        return await data;
    } catch (error) {
        console.error("Error fetching data: ", error);
        return []; // return empty array in case of error
    }
}

export async function GetOrderById(orderId) {
    try {
        const response = await fetch(ORDER_URL + "/" + orderId);
        if (!response.ok) {
            throw new Error('Network response was not ok ' + response.statusText);
        }
        const data = response.json();
        console.log("product: ", data);
        return await data;
    } catch (error) {
        console.error("Error fetching data: ", error);
        return []; // return empty array in case of error
    }
}

export async function getReviewsByProductId(productId) {
    try {
        const response = await fetch(`${REVIEWS_URL}/product/${productId}`);
        if (!response.ok) {
            throw new Error('Network response was not ok ' + response.statusText);
        }
        const data = await response.json();
        console.log("reviews for product: ", data);
        return data; // Предполагается, что это массив
    } catch (error) {
        console.error("Error fetching reviews: ", error);
        return []; // Возвращает пустой массив в случа
    }
}