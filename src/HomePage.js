// HomePage.js
import React from 'react';
import { Link } from 'react-router-dom';

const HomePage = () => {
    return (
        <div className="App-header">
            <h1>Home Page</h1>
            <Link to="/products">
                <button>Go to Products</button>
            </Link>
            <Link to="/orders">
                <button> Go to Orders </button>
            </Link>
        </div>
    );
};

export default HomePage;
