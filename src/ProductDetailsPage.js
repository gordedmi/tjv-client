import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import { GetProductById, getReviewsByProductId } from './GetRequests'; // Убедитесь, что путь до файла правильный

const ProductDetailsPage = () => {
  const [productDetails, setProductDetails] = useState({ orders: [] });
  const [reviews, setReviews] = useState([]); // Состояние для отзывов
  const { productId } = useParams();

  useEffect(() => {
    const fetchProductDetailsAndReviews = async () => {
      try {
        // Получаем детали продукта
        const fetchedProductDetails = await GetProductById(productId);
        setProductDetails(fetchedProductDetails);

        // Получаем отзывы продукта
        const fetchedReviews = await getReviewsByProductId(productId);
        setReviews(fetchedReviews);
      } catch (error) {
        console.error('Error fetching product details or reviews:', error);
        setProductDetails({ ...productDetails, orders: [] }); // Обновляем только заказы в состоянии продукта
        setReviews([]); // Устанавливаем пустой массив для состояния отзывов
      }
    };

    if (productId) {
      fetchProductDetailsAndReviews();
    }
  }, [productId]);

  return (
    <div>
      <h1>Product Details</h1>
      {productDetails ? (
        <>
          <div>
            <h2>{productDetails.name}</h2>
            <p>{productDetails.description}</p>
            <p>Price: ${productDetails.price}</p>
          </div>

          {/* Отображение отзывов */}
          <h3>Reviews</h3>
          {reviews.length > 0 ? (
            reviews.map(review => (
              <div key={review.id}>
                <p>Rating: {review.rating}</p> {/* Убедитесь, что здесь 'rating' */}
                <p>{review.text}</p> {/* Убедитесь, что здесь 'text' */}
              </div>
            ))
          ) : (
            <p>No reviews available for this product.</p>
          )}

          {/* Отображение заказов */}
          <h3>Orders</h3>
          {productDetails.orders && productDetails.orders.map(order => (
            <div key={order.id}>
              <p>Order ID: {order.id}</p>
            </div>
          ))}
        </>
      ) : (
        <p>Loading product details...</p>
      )}

      <Link to={`/products`}>
        <button>Back</button>
      </Link>
    </div>
  );
};

export default ProductDetailsPage;
