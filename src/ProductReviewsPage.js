import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { getReviewsByProductId } from './GetRequests'; // Функция для запроса отзывов о продукте
import { Link } from 'react-router-dom';
const ProductReviewsPage = () => {
    const [reviews, setReviews] = useState([]);
    const { productId } = useParams(); // Получаем ID продукта из URL

    useEffect(() => {
        const fetchReviews = async () => {
            try {
                const fetchedReviews = await getReviewsByProductId(productId);
                setReviews(fetchedReviews);
            } catch (error) {
                console.error("Error fetching reviews:", error);
                // Обработка ошибок
            }
        };

        fetchReviews();
    }, [productId]);

    return (
        <div>
            <Link to="/products" style={{ textDecoration: 'none', marginBottom: '10px' }}>
                <button>Back </button>
            </Link>
            <h1>Product Reviews</h1>
            {reviews.length > 0 ? (
                <ul>
                    {reviews.map((review) => (
                        <li key={review.id}>
                            <p>Rating: {review.rating}</p>
                            <p>{review.text}</p>
                            {/* Дополнительная информация об отзыве */}
                        </li>
                    ))}
                </ul>
            ) : (
                <p>No reviews available for this product.</p>
            )}
        </div>
    );
};

export default ProductReviewsPage;
