import React, { useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

const AddProductReviewPage = () => {
    const { productId } = useParams();
    const navigate = useNavigate();
    const [review, setReview] = useState({ rating: 1, text: '' });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setReview({ ...review, [name]: value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        // Здесь отправляем POST-запрос на сервер с данными отзыва
        // Вам нужно будет имплементировать API-вызов для отправки данных
        try {
            // const response = await addReviewAPI(productId, review);
            console.log('Review submitted for product:', productId, review);
            navigate(`/products/${productId}`); // Вернуться на страницу продукта после отправки
        } catch (error) {
            console.error('Failed to submit review:', error);
            // Обработка ошибок отправки отзыва
        }
    };

    return (
        <div>
            <h1>Add Review for Product ID: {productId}</h1>
            <form onSubmit={handleSubmit}>
                <label>
                    Rating:
                    <select name="rating" value={review.rating} onChange={handleChange}>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </label>
                <label>
                    Review Text:
                    <textarea
                        name="text"
                        value={review.text}
                        onChange={handleChange}
                    />
                </label>
                <button type="submit">Submit Review</button>
            </form>
            <button onClick={() => navigate(-1)}>Cancel</button>
        </div>
    );
};

export default AddProductReviewPage;
