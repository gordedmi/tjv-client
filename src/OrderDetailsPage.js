import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { GetOrderById } from './GetRequests'; // Ensure you have this function implemented to fetch order details
import { Link } from 'react-router-dom';

const OrderDetailsPage = () => {
    const [order, setOrder] = useState(null);
    const { orderId } = useParams();

    useEffect(() => {
        const fetchOrder = async () => {
            try {
                const fetchedOrder = await GetOrderById(orderId);
                setOrder(fetchedOrder);
            } catch (error) {
                console.error("Error fetching order details:", error);
                // Handle error state appropriately
            }
        };

        fetchOrder();
    }, [orderId]);

    if (!order) {
        return <p>Loading order details...</p>;
    }

    return (
        <div>
            <Link to="/orders" style={{ textDecoration: 'none', marginBottom: '10px' }}>
                <button>Back </button>
            </Link>
            <h1>Order Details</h1>
            <h2>Order ID: {order.id}</h2>
            <p>Total Price: ${order.totalPrice.toFixed(2)}</p>
            <p>Date: {new Date(order.date).toLocaleDateString()}</p>
            <p>Status: {order.status}</p>
            <p>Customer: {order.customer ? order.customer.firstName : 'N/A'}</p>
            <p>Address: {order.address}</p>
            <h3>Products in this Order:</h3>
            <ul>
                {order.products.map((product) => (
                    <li key={product.id}>
                        {product.name} - {product.description} - ${product.price}
                    </li>
                ))}
            </ul>
            {/* Implement additional order details as needed */}
        </div>
    );
};

export default OrderDetailsPage;
